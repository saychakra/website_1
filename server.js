var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var app = express();
var port = process.env.PORT || 8080;

//using ejs and express layouts
app.set('view engine', 'ejs');
app.use(expressLayouts);

//routing the app
var router = require('./app/routes');
app.use('/', router);

//setting the static files (css, images etc.)
app.use(express.static(__dirname + '/public'));

//starting the server
app.listen(port, function() {
	console.log(`Server started on port`);	
});