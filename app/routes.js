//require express
var express = require('express');
var path = require('path');
//create the router object
var router = express.Router();

//exporting the router module
module.exports = router;

//route for the homepage
router.get('/', function(req, res) {
	res.render('pages/index');
});

//route for the about page
router.get('/about', function(req, res) {
	res.render('pages/about');
});

router.get('/contact', function (req, res) {
	res.render('pages/contact');
});
router.post('/contact', function (req, res) {
	
});